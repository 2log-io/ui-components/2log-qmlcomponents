import QtQuick 2.5
import UIControls 1.0
import QtQuick.Layouts 1.3

Rectangle {
    id: docroot
    color: Colors.darkBlue //brighterDarkBlue
    radius: 0
    default property alias content: content.children
    property int margins: 20
    property int topMargin: margins
    property int spacing: 20

    Item {
        id: content
        anchors.fill: parent
        anchors.rightMargin: docroot.margins
        anchors.leftMargin: docroot.margins
        anchors.bottomMargin: docroot.margins
        anchors.topMargin: docroot.topMargin
    }

    Shadow {
        visible: docroot.color != "transparent"
    }
}
