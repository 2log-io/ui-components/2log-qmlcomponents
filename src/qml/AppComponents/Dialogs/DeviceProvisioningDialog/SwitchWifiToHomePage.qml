/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.4
import UIControls 1.0
import AppComponents 1.0

Item {
    id: docroot

    property bool active: (StackView.status === StackView.Active)
    property string targetWifi
    signal next
    signal cancel

    visible: StackView.status !== StackView.Inactive

    Column {
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 50
        spacing: 30

        TextLabel {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Jetzt das W-LAN wechseln")
            fontSize: Fonts.headerFontSze
        }


        TextLabel {
            width: parent.width > 500 ? 500 : parent.width
            text: qsTr("Alles klar, wir haben es fast geschafft! Bitte verlasse noch einmal kurz die App und wechsle jetzt wieder zurück in das Wifi deines Routers.")
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.Wrap
        }

        StandardButton
        {
            text: qsTr("Okay, ist erledigt!")
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: docroot.next()
        }


    }

//    StandardButton {
//        transparent: true
//        icon: Icons.cancel
//        anchors.bottom: parent.bottom
//        opacity: parent.active ? 1 : 0
//        text: qsTr("Abbrechen")
//        onClicked: docroot.cancel()

//        Behavior on opacity {
//            NumberAnimation {}
//        }
//    }

//    StandardButton {
//        transparent: true
//        icon: Icons.rightAngle
//        text: qsTr("Weiter")
//        anchors.bottom: parent.bottom
//        anchors.right: parent.right
//        opacity: parent.active ? 1 : 0
//        iconAlignment: Qt.AlignRight
//        onClicked: docroot.next()

//        Behavior on opacity {
//            NumberAnimation {}
//        }
//    }
}
